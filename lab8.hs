
fact :: Int -> Int
fact 1 = 1
fact n = n * fact (n-1)

fib :: Int -> Int
fib n | n < 2 = 1
      | True = fib (n-1) + fib (n-2)

take' :: Int -> [a] -> [a]
take' 1 (x:_) = [x]
take' n (x:xs) = [x] ++ take' (n-1) xs

fib2 :: Integral a => a -> [a] -> a
fib2 0 [a, b] = b
fib2 n [a, b] = fib2 (n-1) [b, (a+b)]

fibreal n = fib2 n [0, 1]

fiblist l1 l2 = l2:fiblist l2 (l1+l2)

fiblistreal = fiblist 0 1
