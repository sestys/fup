import SedmaDatatypes

dd = [Card Club RA,Card Club RK,Card Club RQ,Card Club RJ,Card Club R10,Card Club R9,Card Club R7,Card Club R8,Card Spade RA,Card Spade RK,Card Spade RQ,Card Spade RJ,Card Spade R10,Card Spade R9,Card Spade R7,Card Spade R8,Card Diamond RA,Card Diamond RK,Card Diamond RQ,Card Diamond RJ,Card Diamond R7,Card Diamond R9,Card Diamond R8,Card Diamond R10,Card Heart RJ,Card Heart RK,Card Heart RQ,Card Heart RA,Card Heart R7,Card Heart R9,Card Heart R8,Card Heart R10]

instance Eq Rank where
    R7 == R7 = True
    R8 == R8 = True
    R9 == R9 = True
    R10 == R10 = True
    RJ == RJ = True
    RQ == RQ = True
    RK == RK = True
    RA == RA = True
    _ == _ = False

instance Eq Card where
    Card s1 r1 == Card s2 r2 = r1 == r2

replay :: Cards -> Maybe Winner
replay [] = Nothing
replay deck | length deck /= 32 = Nothing
            | otherwise = Just (simGame deck AC 0 0) 

nextPlayer :: Team -> Team
nextPlayer AC = BD
nextPlayer BD = AC

countRound :: Cards -> Int -> Int
countRound [] count = count
countRound (card:cards) count | card == Card Club RA || card == Card Club R10 = countRound cards (count + 10)
                              | otherwise = countRound cards count


getRoundWinner :: Cards -> Card -> Team -> Team -> Team
getRoundWinner [] _ winner _ = winner
getRoundWinner (card:cards) firstCard winner player | card == firstCard = getRoundWinner cards firstCard player (nextPlayer player) 
                                                    | card == Card Club R7 = getRoundWinner cards firstCard player (nextPlayer player)
                                                    | otherwise = getRoundWinner cards firstCard winner (nextPlayer player)

roundWinner :: Cards -> Team -> Team
roundWinner (card:cards) player = getRoundWinner cards card player (nextPlayer player)

getRound :: Cards -> Cards
getRound [] = []
getRound cards = take 4 cards

endRound :: Cards -> Cards
endRound cards = drop 4 cards

getWinner :: Int -> Int -> Winner
getWinner 0 _ = (BD, Three)
getWinner _ 0 = (AC, Three)
getWinner sA sB | sA < 10 && sA < sB = (BD, Two)
                | sA < sB = (BD, One)
                | sB < 10 && sB < sA = (AC, Two)
                | sB < sA = (AC, One)
 
simGame :: Cards -> Team -> Int -> Int -> Winner
simGame [] lastWiner scoreA scoreB | lastWiner == AC = getWinner (scoreA + 10) scoreB
                                   | otherwise = getWinner scoreA (scoreB + 10)
simGame cards lastWinner scoreA scoreB = simGame restDeck winner sA sB where
            restDeck = endRound cards
            round = getRound cards
            winner = roundWinner round lastWinner
            sA | winner == AC = scoreA + (countRound round 0) + 1
               | otherwise = scoreA
            sB | winner == BD = scoreB + (countRound round 0) + 1
               | otherwise = scoreB

