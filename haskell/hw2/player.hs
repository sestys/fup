import SedmaBase
import SedmaReplay
import SedmaGamble
import SedmaDecks

data MyState = MyState {
            pl:: Player,
            hand::Hand,
            history::Cards,
            currentRound::Int
} deriving (Show)



instance PlayerState MyState where
    initState p h = MyState p h [] 1
    updateState trick trickLead mycard newcard state = MyState player' hand' history' round' where
            player' = pl state
            hand' = [x | x <- (hand state), x /= mycard] ++ (getMaybeCard newcard)
            history' = (history state) ++ trick
            round' = currentRound state + 1

getMaybeCard :: Maybe Card -> Cards
getMaybeCard Nothing = []
getMaybeCard (Just card) = [card]

playerSilly :: AIPlayer MyState
playerSilly _ state = (hand state) !! 0

playerBasic :: AIPlayer MyState
playerBasic trick state 
        | length trick >= 1 = findCard (hand state) (trick !! 0) ((hand state) !! 0)
        | otherwise = (hand state) !! 0

player :: AIPlayer MyState
player trick state
        | currentRound state == 8 = (hand state) !! 0
        | len == 0 = playFirst trick state
        | len == 1 = playSecond trick state
        | len == 2 = playThird trick state
        | len == 3 = playFourth trick state
        where
            len = length trick


isRankedCard :: Card -> Bool
isRankedCard (Card _ r) = (r == R10) || (r == RA)

isUnimportantCard :: Card -> Bool
isUnimportantCard c@(Card _ r) = not (isRankedCard c) && (r /= R7)

playFirst :: AIPlayer MyState
playFirst _ state = cc !! 0 where
            hs = hand state
            cs = [x | x <- hs, isUnimportantCard x]
            cc | length cs == 0 = hs
               | otherwise      = cs

playSecond :: AIPlayer MyState
playSecond trick state = cc !! 0 where
            hs = hand state
            fCard = trick !! 0
            cs = [x | x <- hs, beatsNoSeven x fCard]
            cn = [x | x <- hs, beats x fCard]
            cc | length cn == 0 = [playFirst trick state]
               | length cs /= 0 = cs
               | otherwise      = cn

playThird :: AIPlayer MyState
playThird trick state = playFourth trick state

playFourth :: AIPlayer MyState
playFourth trick state | isWinning trick = cc !! 0
                       | otherwise       = playSecond trick state
                       where
                         hs = hand state
                         cs = [x | x <- hs, isRankedCard x]
                         cc | length cs == 0 = [playFirst trick state]
                            | otherwise      = cs

beatsNoSeven :: Card -> Card -> Bool
beatsNoSeven (Card _ r1) (Card _ r2) = r1 == r2

isWinning :: Trick -> Bool
isWinning [] = True
isWinning trick | len == 1 = False
                | len == 2 = not (beats secondC firstC)
                | len == 3 = not (beats thirdC firstC) && (beats secondC firstC)
                where
                    len = length trick
                    firstC  = trick !! 0
                    secondC = trick !! 1
                    thirdC  = trick !! 2


findCard :: Cards -> Card -> Card -> Card
findCard [] card selected = selected
findCard (c:cs) card selected
        | beats c card = c
        | otherwise    = findCard cs card selected  

simGames :: [Cards] -> [Maybe Winner] -> Int 
simGames [] results = countDifference results 0
simGames (deck:decks) results = simGames decks results' where
    results' = results ++ [replay (gamble player playerBasic deck)]

countDifference :: [Maybe Winner] -> Int -> Int
countDifference [] c = c
countDifference (w:ws) c = countDifference ws c' where
        c' = c + (count w)


--main = print (simGames decks [])
