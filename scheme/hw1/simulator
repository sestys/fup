#lang r5rs

;(require racket/trace)
(define get-maze
'(
(w w w w w w)
(w 1 w 0 w w)
(w 0 w 0 0 w)
(w 0 0 0 w w)
(w w w w w w)
)
)

(define get-maze2
'(
(w w w)
(w 0 w)
(w w w)
)
)
(define get-maze3
'(
(w w w)
(w 0 w)
(w 0 w)
(w w w)
)
)

(define get-maze4
'(
(w w w w w w)
(w 0 0 0 0 w)
(w w w w w w)
)
)

(define turn-north
  '(
    (procedure turn-north
      (if north?
          ()
          (turn-left turn-north)
      )
     )
    )
)


(define get-state
  (list get-maze (list 1 1) 'south))

(define right-hand-rule-prg
  '(
    (procedure start
      ( turn-right
        (if wall?
           ( turn-left
             (if wall?
                 (turn-left
                     (if wall?
                        turn-left
                        step
                     )
                 )
                 step
              )
           )
           step  
        )
        put-mark
        start
      )
    )   
    (procedure turn-right (turn-left turn-left turn-left))
  )
)
(define program
  '(
    (procedure setup
      (if north? (if mark? (get-mark setup) ()) (turn-left setup))
    )
    )
)


(define (subst l n x)
  (apply-at (lambda (y) x) l n))

(define (apply-at fn lst pos)
  (cond ((= pos 0) (cons (fn (car lst)) (cdr lst)))
        (#t (cons (car lst) (apply-at fn (cdr lst) (- pos 1))))
        )
)

(define (apply-xy fn x y list-of-lists)
  (apply-at (lambda (line)
              (apply-at fn line x)
             ) list-of-lists y)
)

(define (check-at fn lst pos)
  (cond ((= pos 0) (fn (car lst)))
        (#t (check-at fn (cdr lst) (- pos 1)))
        )
  )
(define  (check-xy fn x y list-of-list)
  (check-at (lambda (line) (check-at fn line x)) list-of-list y)
  )

(define (maze-state state)
  (car state))

(define (coor-state state)
  (cadr state))

(define (orientation-state state)
  (if (null? (cdr state)) (car state) (orientation-state (cdr state))))

;;; coor = (x y)
(define (wall? state)
  (define (w? x)
    (eqv? x 'w))
  (let ((coor (coor-state state))
        (orientation (orientation-state state))
        (maze (maze-state state)))
    (cond ((eqv? orientation 'north)(check-xy w? (car coor) (- (cadr coor) 1) maze))
          ((eqv? orientation 'south)(check-xy w? (car coor) (+ (cadr coor) 1) maze))
          ((eqv? orientation 'east)(check-xy w? (+ (car coor) 1) (cadr coor) maze))
          ((eqv? orientation 'west)(check-xy w? (- (car coor) 1) (cadr coor) maze))
          (#t 'invalid)
          )
    )
)

(define (mark? state)
  (let ((maze (maze-state state))
        (coor (coor-state state)))
    (check-xy (lambda (x) (> x 0)) (car coor) (cadr coor) maze)
    )
  )

(define (north? state)
  (let ((orientation (orientation-state state)))
    (eqv? orientation 'north)
    )
  )

(define (step state)
  (define (get-coor orientation coor)
    (cond ((eqv? orientation 'north)(list (car coor) (- (cadr coor) 1)))
        ((eqv? orientation 'south)(list (car coor) (+ (cadr coor) 1)))
        ((eqv? orientation 'east)(list (+ (car coor) 1) (cadr coor)))
        ((eqv? orientation 'west)(list (- (car coor) 1) (cadr coor)))
        ))
  (let ((orientation  (orientation-state state))
        (coor (coor-state state))
        (maze (maze-state state)))
    (list maze (get-coor orientation coor) orientation)  
      )
  )

(define (turn-left state)
  (let ((orientation (orientation-state state)))
    (cond ((eqv? orientation 'north) (subst state 2 'west))
          ((eqv? orientation 'south) (subst state 2'east))
          ((eqv? orientation 'east) (subst state 2'north))
          ((eqv? orientation 'west) (subst state 2'south))
          )
    )
  )

(define (get-mark state)
  (let ((maze (maze-state state))
        (coor (coor-state state))
        (orientation (orientation-state state)))
    (list (apply-xy (lambda (x) (- x 1)) (car coor) (cadr coor) maze) coor orientation)
    )
  )

(define (put-mark state)
  (let ((maze (maze-state state))
        (coor (coor-state state))
        (orientation (orientation-state state)))
    (list (apply-xy (lambda (x) (+ x 1)) (car coor) (cadr coor) maze) coor orientation)
    )
  )
      


(define (simulate-list state expr program actSeq)
  (cond ((null? expr) (list actSeq state))
        (#t (cond ((equal? (car expr) 'step) (if (wall? state)
                                               (list actSeq state)
                                               (simulate-list (step state) (cdr expr) program (append actSeq (list (car expr))))))
                  ((equal? (car expr) 'turn-left) (simulate-list (turn-left state) (cdr expr) program (append actSeq (list (car expr)))))
                  ((equal? (car expr) 'put-mark) (simulate-list (put-mark state) (cdr expr) program (append actSeq (list (car expr)))))
                  ((equal? (car expr) 'get-mark) (if (mark? state)
                                                     (simulate-list (get-mark state) (cdr expr) program (append actSeq (list (car expr))))
                                                     (list actSeq state)))
                  ((equal? (car expr) 'if) (simulate-list state (eval-if expr state) program actSeq))
                  (#t (let ((ret (simulate-list state (get-procedure-by-name (car expr) program) program actSeq)))
                        (simulate-list (cadr ret) (cdr expr) program (car ret))))

                  )
            )
        )
  )

;(define (simulate-procedure state


(define (get-procedure-by-name name program)
  (cond ((null? program) '())
        (#t (if (equal? name (cadr (car program)))
                (car (cdr (cdr (car program))))
                (get-procedure-by-name name (cdr program))
                )
            )))

;(trace simulate-list)
;;; state = (maze (x y) orientation)
(define (simulate state expr program limit) 
  (cond ((null? expr) (list '() state))
        ((list? expr) (simulate-list state expr program '()))
        (#t (simulate-list state (list expr) program '()))
  )
 )

(define (eval-condition con state)
  (cond ((equal? con 'north?) (north? state))
        ((equal? con 'mark?) (mark? state))
        ((equal? con'wall?) (wall? state))
        )
  )

;;takes if and returns the branch that should be executed
(define (eval-if program state)
  (cond ((equal? (car program) 'if) (if (eval-condition (cadr program) state)
                                        (list-ref program 2)
                                        (list-ref program 3)))
        (else program)
                                        
      ))




