module BST where

data MyList a = Nil | Cons a (MyList a)

instance (Show a) => Show (MyList a) where
  show Nil = ""
  show (Cons a Nil) = show a
  show (Cons a b) = show a ++ "," ++ (show b)

instance (Eq a) => Eq (MyList a) where
  Nil == Nil = True
  (Cons a b) == (Cons c d) = a == c && b == d
  _ == _ = False
 
lenght::MyList a -> Int
lenght Nil = 0
lenght (Cons a b) = 1 + lenght b

list2my::[a] -> MyList a
list2my [] = Nil
list2my (x:xs) = Cons x (list2my xs)

data BST a = BSTNil | BSTNode a (BST a) (BST a) deriving Show


list2bst::(Ord a) => [a] -> BST a
list2bst [] = BSTNil
list2bst (x:xs) = BSTNode x (list2bst [y | y <- xs, y < x]) (list2bst [y | y <- xs, y >= x])

(+=)::(Ord a) => BST a -> a -> BST a
BSTNil += a = BSTNode a BSTNil BSTNil
p@(BSTNode a b c) += d | d < a = BSTNode a (b += d) c
                     | d > a = BSTNode a b (c += d)
                     | otherwise = p

treeFold::(a -> b -> b -> b) -> b -> (BST a) -> b
treeFold _ e BSTNil = e
treeFold f e (BSTNode a b c) = f a (treeFold f e b) (treeFold f e c)

sumTree::(Num a)=> BST a -> a
sumTree = treeFold (\x y z -> x+y+z) 0

depthTree:: BST a -> Int
depthTree = treeFold (\_ y z -> 1 + (max y z)) 0


isBalanced :: BST a -> Bool
isBalanced BSTNil = True
isBalanced (BSTNode _ l r) = isBalanced l && isBalanced r && abs ((depthTree l) - (depthTree r)) <= 1

instance Functor BST where
    fmap _ BSTNil = BSTNil
    fmap f (BSTNode a b c) = BSTNode (f a) (fmap f b) (fmap f c)
